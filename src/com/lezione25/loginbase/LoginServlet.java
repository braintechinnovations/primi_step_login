package com.lezione25.loginbase;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet{

	private String user_autorizzato = "ciccio";
	private String pass_autorizzato = "pasticcio";
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String var_username = request.getParameter("username");
		String var_password = request.getParameter("password");
		
		HttpSession sessione = request.getSession();
		
		if(sessione.getAttribute("isAuth") != null && (boolean)sessione.getAttribute("isAuth") == true) {
			response.sendRedirect("profilo");
			return;
		}
		
		if(var_username.equals(this.user_autorizzato) && var_password.equals(pass_autorizzato)) {
			sessione.setAttribute("isAuth", true);
			response.sendRedirect("profilo");
		}
		else {
			response.sendRedirect("errorelogin.html");
		}
		
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.print("NON PUOI ACCEDERE A QUESTA FUNZIONE!");
	}
	
}
