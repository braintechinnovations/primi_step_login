package com.lezione25.loginbase;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ProfiloServlet extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession sessione = request.getSession();
		
		if(sessione.getAttribute("isAuth") != null && (boolean)sessione.getAttribute("isAuth") == true) {
			PrintWriter out = response.getWriter();
			out.print("SEI LOGGATO :)");
		}
		else {
			response.sendRedirect("errorelogin.html");
		}
		
	}
	
}
